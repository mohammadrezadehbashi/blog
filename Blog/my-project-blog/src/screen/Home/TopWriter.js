import React, { useEffect, useState } from "react";


function TopWriter() {

    const [listWriter, setListW] = useState([]);

    useEffect(() => {
        getWritersHandle();
    }, [])

    const getWritersHandle = () => {
        try {
            fetch("http://localhost:4000/user/top-users")
                .then(response => response.json())
                .then((data) => {
                    setListW(data);
                    // setLoadingw(false);
                }
                )
        } catch (error) {
            console.error(error);
        }

    }
   
    console.log("zzx",listWriter);

    return (
        <>
            {listWriter.map((item) => (
                <div class="w-full flex bg-sky-900 h-28 my-8 semi-xl:-my-0 lg:grid lg:h-60 sm:flex sm:h-24 sm:w-36 sm:mr-1 sm:items-center ">
                    <img src={'./assets/images/The Mask wallpaperz.blog.ir .jpeg'} class="mx-4 h-40 w-24 -my-6 semi-xl:h-full semi-xl:-my-0 semi-xl:mx-0 lg:w-full lg:h-24 up-semi-sm:w-1/2"/>
                    {/* triagle */}
                    <div class=" w-20 overflow-hidden inline-block lg:w-full ">
                        <div class=" h-11 w-11 bg-rose-400 -rotate-45 transform origin-top-left lg:hidden"></div>
                        <div class="sm:ml-0 ">
                            <p class="mx-2 w-14 -mt-5 font-medium lg:text-center lg:-mt-0 lg:ml-2 lg:w-44 sm:ml-1 sm:w-10 sm:text-sm up-semi-sm:text-xs">{item.name}</p>
                            <div class="sm:hidden">
                                <p class="text-right text-xs lg:ml-2  ">{item.bio.substring(0,12)}</p>
                            </div>
                        </div>
                    </div>
                </div>
            ))}
        </>
    )
}

export default TopWriter