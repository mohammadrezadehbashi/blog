import React, { useEffect, useState } from "react";
import "./Home.css";
import { Outlet, Link, useParams } from "react-router-dom";
import TopWriter from "./TopWriter";
import SimpleSlider from "../component/Slider";
import { Counter } from "../../features/Counter";
import TopBloggg from "../../features/TopBloggg";

const Home = () => {
  const { _id } = useParams();

  const [loading, setLoading] = useState(true);
  const [loadingw, setLoadingw] = useState(true);
  const [listW, setListW] = useState([]);


  const getWriters = () => {
    try {
      fetch("http://localhost:4000/user/top-users")
        .then((response) => response.json())
        .then((data) => {
          setListW(data);
          // setLoadingw(false);
        });
    } catch (error) {
      console.error(error);
    }
  };
  useEffect(() => {
    getWriters();
  }, []);

  // if (loadingw) return <h1>loading</h1>

  return (
    <>
      <div class="h-96 my-5 bg-[url('/src/assets/images/toy-bricks-table-with-word-blog.jpg')] bg-cover bg-fixed">
        <div class="pt-24">
          <SimpleSlider />
        </div>
      </div>

      <div class="grid grid-cols-4  gap-10 mx-24 mb-10 lg:gap-4 md:mx-12 up-semi-sm:mx-4 semi-sm:gap-1 semi-sm:flex">
        {/* left */}
        <div class="col-span-3  sm:col-start-1 sm:col-end-5">
          <div class="w-full py-24 bg-blue-400 ">
            <h1 class="text-center text-6xl">مقالات</h1>
          </div>

          <div class="my-2 border-b-2 border-b-stone-500"></div>

          <div class=" flex gap-4 semi-sm:grid">
            {/* colomn1 */}
            <div class="w-full ">
              <div class="flex mb-2 semi-lg:grid semi-sm:flex xs:grid">
                <img
                  src={'./assets/images/images__1.png'}
                  class="w-1/2 semi-lg:w-96 semi-lg:h-40 semi-sm:h-auto xs:h-40"
                />
                <span class="w-full bg-white p-4">
                  <h1 class="font-serif font-normal tracking-wide text-xl">
                    <strong class="font-bold">Joker</strong> In Fire
                  </h1>
                  <hr class="border-slate-600 m-2"></hr>
                  <p class="font-serif text-left indent-4 text-sm">
                    Forever alone in a crowd, failed comedian Arthur Fleck seeks
                    connection as he walks the streets of Gotham City. Arthur
                    wears two masks -- the one he paints for his day job as a
                    clown, and the guise he projects in a futile attempt to feel
                    like he's part of the world around him.
                  </p>
                </span>
              </div>
              <div class="bg-white">
                <img src={'./assets/images/632833413-talab-ir.png'} class="" />
                <div class="p-2">
                  <h1 class="font-serif font-normal tracking-wide text-xl">
                    Exciting <strong class="font-bold">driving</strong> out of
                    <br />
                    town
                  </h1>
                  <hr class="border-slate-600 m-2"></hr>
                  <p class="font-serif text-left indent-2 text-sm">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Maxime mollitia, molestiae quas vel sint commodi repud
                    iandae consequun tur vol uptatum laborum numquam bl anditiis
                    h aliquid.anditiis.Voluptatem quaerat non architecto ab
                    laudantium modi minima sunt esse temporibus sint culpa,
                    recusandae aliquam numquam totam ratione voluptas quod
                    exercitationem fuga. Possimus quis earum veniam quasi
                    aliquam eligendi, placeat qui corporis! recusandae aliquam
                    numquam recusandae aliquam
                  </p>
                </div>
              </div>
            </div>

            {/* colomn2 */}
            <div class="w-full  overflow-hidden">
              <div class="mb-2">
                <img src={'./assets/images/games_hd_wallpapers_downloadha_31.jpg'} class="w-full" />
                <div class="p-2 bg-white">
                  <h1 class="font-serif font-normal tracking-wide text-xl">
                    <strong class="font-bold">Game</strong> position in Asia
                  </h1>
                  <hr class="border-slate-600 m-2"></hr>
                  <p class="font-serif text-left indent-2 text-sm">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Maxime mollitia, molestiae quas vel sint commodi repud
                    iandae consequun tur vol uptatum laborum numquam bl anditiis
                    h aliquid.anditiis.
                  </p>
                </div>
              </div>
              <div class="flex semi-lg:grid">
                <img src={'./assets/images/The Mask wallpaperz.blog.ir .jpeg'} class="w-1/2 semi-lg:w-full" />
                <div class="p-2 bg-white">
                  <h1 class="font-serif font-bold tracking-wide text-xl">
                    Attractive
                  </h1>
                  <hr class="border-slate-600 m-2"></hr>
                  <p class="font-serif text-left indent-2 text-sm">
                    This gritty 1960s drama depicts the lives of a U.S. platoon
                    fighting its way across Europe during World War II. The show
                    is a relatively realistic portrait of the fighting, refusing
                    to glamorize either the soldiers or the war itself. Instead,
                    it concentrates on the struggles of the soldiers to maintain
                    their own humanity.
                  </p>
                </div>
              </div>
              <div class=" w-full my-2 h-full bg-white">
                <Counter />
                <TopBloggg />
                <p class="text-center  py-10 text-2xl font-bold">Blog</p>
                <div class=""></div>
              </div>
            </div>
          </div>

          <div class="bg-white hidden sm:block sm:flex sm:mx-12 semi-sm:hidden">
            <TopWriter />
          </div>
        </div>

        {/* right */}

        <div class="grid col-start-4 col-end-5 p-1 bg-white sm:hidden">
          <TopWriter />
          <div class="bg-white w-full sm:hidden">
            <div class="p-2">
              <img src={'./assets/images/images__1.png'} class="h-full w-full pb-6" />
              <h1 class="font-bold">Joker In Time</h1>
              <p class="font-semibold">hdihdigf wgyuwg gwfg fguwfg </p>
            </div>
          </div>
        </div>
      </div>


    </>
  );
};

export default Home;

