import React from "react"
import { Link, useNavigate, useParams } from "react-router-dom"
import Cookies from "universal-cookie"
import { useState } from "react";
import { useEffect } from "react";
import { userGet } from "../../services/userr"
import { toast } from "react-toastify";


const HeaderList = [
    ['لیست مقالات', '/blogs'],
    ['لیست نویسندگان', '/users'],
    ['ثبت نام', '/SignUp'],
    ['پنل کاربری', "/Dashboard/profile/edit"]
]

const cookies = new Cookies();


const Header = () => {
    const { _id } = useParams();
    const navigate = useNavigate();


    let tokenCookie = cookies.get('token')

    const [token, setToken] = useState(tokenCookie)

    useEffect(() => {
        console.log("HEEEELO");
    }, [token])

    const removeTokenHandle = () => {
        setTimeout(()=>{
            cookies.set("token", "")
            setToken(cookies.get("token"))
            // toast('خداحافظ')
        },1000)
    }


    const userFetch = () => {
        // userGet(tokenCookie).then((data)=>console.log("alam1",data))  
    
    }


    return (
        <div class="bg-white">
            <div class="grid grid-rows-1 grid-cols-9 grid-flow-col gap-4 px-12 py-5 border-b-2 xs:py-3">
                <Link to="/" class="bg-rose-400 w-16 xs:col-start-4 xs:col-span-1">
                    <img src={'assets/svg/blogger-svgrepo-com.svg'} class="w-16 h-16" />
                </Link>
                    {token === ""||token === null||token === undefined ?
                    <div class="bg-blue-200 col-start-9 justify-self-end px-5 py-3 rounded-xl ring-2 mb-4
                    hover:bg-green-400 hover:text-white hover:ring-0 xs:px-1 xs:h-8 ">
                        <Link to="/Login" class="">
                            <p class="text-center font-bold xs:font-base xs:text-xs xs:-mt-1.5">ورود</p>
                        </Link>
                        </div>
                        :
                        <div  onClick={removeTokenHandle} class="bg-blue-200 col-start-9 justify-self-end px-5 py-3 rounded-xl ring-2 mb-4
                hover:bg-green-400 hover:text-white hover:ring-0 xs:px-1 xs:h-8 ">
                            <p class="text-center font-bold xs:font-base xs:text-xs xs:-mt-1.5">خروج</p>
                        </div>
                    }
            </div>
            <div class="flex px-12 py-5 justify-evenly border-b-2 xs:px-1">
                {HeaderList.map(([title, url]) => (
                    <Link to={url}>
                        <h5 class="text-xl font-bold up-semi-sm:text-base semi-sm:text-xs xs:text-[0.50rem]">{title}</h5>
                    </Link>
                ))}
            </div>
        </div>
    )
}

export default Header