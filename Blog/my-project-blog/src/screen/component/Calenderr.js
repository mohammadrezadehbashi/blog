import React, { useState } from 'react';
import Calendar from 'react-calendar';
import "./Calenderr.css"

export default function Calendarr() {

  const [value, onChange] = useState(new Date());

  return (
    <div class="flex justify-center mt-16 border-2 border-teal-700 m-2">
      <Calendar className="cal" onChange={onChange} value={value} />
    </div>
  );
}


