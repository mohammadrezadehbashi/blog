import React from "react"
import { Link } from "react-router-dom"


const Footer = () => {
    return (
        <div class="bg-white">
            <div class="grid grid-cols-5 gap-4 px-16 py-5 md:px-6 xs:px-2">
                <div class=" col-span-2 row-start-1 row-span-1 md:col-span-3">
                    <img src={'/assets/svg/blogging-blogger.svg'} class="w-24 h-24 md:w-16 md:h-16 up-semi-sm:w-10 up-semi-sm:h-10"></img>
                    {/* <div class="mb-4 "><p>logo</p></div> */}
                    <div class=""><p class="leading-7 text-sm md:leading-4 up-semi-sm:text-xs">Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia,
                        molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum
                        numquam blanditiis harum quisquam eius sed odit fugiat iusto fuga praesentium
                        optio, eaque rerum!</p></div>
                </div>


                {/* <div class="bg-lime-200 col-start-3 row-span-4">
                    <div class=" pb-4 row-start-1"><p class="text-center font-bold">INFORMATION</p>
                    </div>
                   <div class=" bg-rose-200 row-start-3 leading-7">
                    {[
                        ['Blog List', '/SubmitBlog'],
                        ['Blog List', '/SubmitBlog'],
                        ['Blog List', '/SubmitBlog'],
                        ['Blog List', '/SubmitBlog'],
                    ].map(([title,url])=>(
                    <Link to={url}><h6 class="text-center font-normal text-stone-500 ">{title}</h6></Link>
                   ) )}
                    </div>
                </div> */}


                <div class="col-start-4 up-semi-sm:hidden">
                    <div class=" pb-4 row-start-1 up-semi-sm:pb-0"><p class="text-center font-bold">ACOUNT</p>
                    </div>
                    <div class="leading-10 md:leading-8">
                        {[
                            ['Login', '/SubmitBlog'],
                            ['Signup', '/SubmitBlog'],
                            ['About Us', '/SubmitBlog'],
                            ['Ruls', '/SubmitBlog'],
                        ].map(([title, url]) => (
                            <Link to={url}><h6 class="text-center font-normal text-stone-500 up-semi-sm:text-xs  up-semi-sm:mb-4">{title}</h6></Link>
                        ))}
                    </div>
                </div>


                <div class="col-start-5 up-semi-sm:col-start-4  up-semi-sm:col-span-2">
                    <div class=" pb-4 row-start-1 up-semi-sm:pb-0"><p class="text-center font-bold">CONTACTS</p>
                    </div>
                    <div class=" px-1 leading-10 md:leading-8 ">
                        {[
                            ['Instagram', '/assets/svg/icons8-instagram.svg', '/SubmitBlog'],
                            ['Tweeter', './assets/svg/icons8-twitter.svg', '/SubmitBlog'],
                            ['Youtube', '/assets/svg/icons8-youtube.svg', '/SubmitBlog'],
                            ['Email', '/assets/svg/icons8-gmail.svg', '/SubmitBlog'],
                        ].map(([title, icon, url]) => (
                            <Link to={url} class="bg-slate-800">
                                <div class="flex justify-between">
                                    <img src={icon} class="w-6 h-6 mt-2 up-semi-sm:mt-1"></img>
                                    <div class="md:mt-3 up-semi-sm:mt-2">
                                        <p class="md:text-sm up-semi-sm:text-xs">{title}</p>
                                    </div>
                                </div>
                            </Link>
                        ))}

                    </div>
                </div>

            </div>
        </div>
    )
}

export default Footer