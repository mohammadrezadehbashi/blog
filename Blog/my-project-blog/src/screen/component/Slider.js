import React, { Component, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useParams } from "react-router-dom";
import Slider from "react-slick";
import { getTopBlogg } from "../../features/topblog/topBlogSlice"



export default function SimpleSlider() {
    const dispatch = useDispatch();
    // const postLists = useSelector(({posts: { postLists } })=>postLists);
    const postLists = useSelector((state) => state.posts.postLists);
    // useEffect(() => {
    //     dispatch(getTopBlogg());
    // }, [])

    const [list, setList] = useState([]);

    useEffect(() => {
        getBlogTopSlider()
    }, [])

    const getBlogTopSlider = () => {
        try {
            fetch("http://localhost:4000/blog/top-blogs")
                .then(response => response.json())
                .then((data) => {
                    // this.state.setState(data);
                    setList(data);

                }
                )
        } catch (error) {
            console.error(error);
        }
    }



    const settings = {
        dots: true,
        infinite: true,
        speed: 1000,
        slidesToShow: 1,
        slidesToScroll: 1,

    };
    return (
        <div style={{ flex: 1 }}>
            <Slider {...settings} style={{ backgroundColor: "",color:'red', marginInline: "10%", paddingTop: "1%" }}>


                {list.map((item, index) => {
                    return (

                        <div>
                            <Link to={`/blog/${item._id}`}>
                                <div class="">
                                    <p class="text-center text-5xl font-extrabold  text-white">{item.title}</p>
                                    <div class="flex justify-center mt-20 mb-5">
                                        <div class="py-2 px-10 rounded-md bg-slate-100/60 ring-1 ring-slate-500 shadow-lg shadow-black">
                                            <p class="font-medium text-black">نمایه</p>
                                        </div>
                                    </div>
                                </div>
                            </Link>
                        </div>
                        
                        )
                })
                    }



            </Slider>

        </div>
    );
}



export function ItemSlider() {
    let { _id } = useParams();
    const dispatch = useDispatch();
    // const postLists = useSelector(({posts: {postLists} })=>postLists);
    const postLists = useSelector((state) => state.posts.postLists);
    useEffect(() => {
        dispatch(getTopBlogg());
    }, [])

    return (
        <div>
            {postLists.map((item) => {
                return (
                    <div key={item._id} class="grid grid-cols-2 bg-sky-100 overflow-hidden m-2 border-l-4 border-rose-600">

                        <div class=" grid grid-cols-4" >
                            <div class="mt-2 col-start-2">
                                <Link to="">
                                    <p class="">{item.title}</p>
                                </Link>
                            </div>
                        </div>

                    </div>
                )
            })}
        </div>
    )
}