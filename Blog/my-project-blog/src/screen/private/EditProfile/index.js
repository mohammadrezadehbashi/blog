import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import Cookies from "universal-cookie";
import { toast } from "react-toastify";
import "react-toastify/ReactToastify.min.css";
const cookies = new Cookies();

const EditProfile = (props) => {
  const { _id } = useParams();

  const [name, setName] = useState("");
  const [type, setType] = useState();
  // const [maxLength, setMaxLength] = useState(200);


  const handleName = (e) => {
    setName(e.target.value);
  };

  const handleType = (e) => {
    setType(e.target.value);
  };

  // const [writer, setwriter] = useState(null);

  const getUser = () => {
    console.log("name", name);
    const token = cookies.get("token");

    fetch("http://localhost:4000/user/me", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        auth: token ? `ut ${token}` : null,
      },
      body: "{}",
    })
      .then((response) => response.json())
      .then((data) => {
        // console.log("dddddd", data)
        if (data) {
          setName(data.name);
          setType(data.bio);
          // setLoading(false)
        }
      });
  };

  useEffect(() => {
    getUser();
  }, []);



  const handleEdit = () => {
    const token = cookies.get("token");

    console.log("tokenInedit:", token);
    fetch("http://localhost:4000/user/edit", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        auth: token ? `ut ${token}` : null,
      },
      body: JSON.stringify({
        name: name,
        bio: type,
      }),
    })
      .then((response) => response.json())
      .then(
        (data) => {
          console.log("Edit**", data);
          toast("ویرایش انجام شد");
        }
        //  setName(name),
        //  setBioString(bioString)
      );
  };

  /////////////////////////////////////////update-avatar

  const [file, setfile] = useState(null);
  //   const [filebase, setfilebase] = useState(null);

  const token = cookies.get("token");

  //CONVERT TO BASE64
  const getBase64 = (file) => {
    return new Promise((resolve) => {
      let fileInfo;
      let baseURL = "";
      // Make new FileReader
      let reader = new FileReader();
      // Convert the file to base64 text
      reader.readAsDataURL(file);

      // on reader load somthing...
      reader.onload = () => {
        // Make a fileInfo Object
        console.log("Called", reader);
        baseURL = reader.result;

        //for display

        console.log("baseurl", baseURL);
        resolve(baseURL);
      };
    });
  };

  //ONCHANGE FOR IMAGE
  const handleFileInputChange = (e) => {
    console.log(e.target.files[0]);
    let file;

    file = e.target.files[0];

    console.log("func:::", file);

    getBase64(file)
      .then((result) => {
        console.log("File Is", file);
        console.log("result Is", result);

        setfile(result.substr(22));

      })
      .catch((err) => {
        console.log("errrr", err);
      });

    setfile(e.target.files[0]);

  };
  console.log("UP file", file);
  localStorage.setItem("file", file);

  const submitAvatar = async () => {
    try {

      setfile(localStorage.getItem("file"));

      const formData = new FormData();
      formData.append("avatar", file);

      console.log(
        "filllllleeeee",
        formData,
        token,
        localStorage.getItem("file")
      );

      await fetch("http://localhost:4000/user/update-avatar", {
        method: "POST",
        headers: {
          // 'Content-Type': 'application/json',
          auth: token ? `ut ${token}` : null,
        },
        body: formData,
      }).then((res) => {
        //   res.json();
        console.log("yessssss", res);
      });
      // .then((data) => {
      //   console.log("xx", data);
      // });

      console.log("formdata", formData);
    } catch (error) {
      console.log("lol");
    }
  };

  console.log("end file", file);

  return (
    <div class="bg-white w-full h-screen pt-8">
      <div class="flex justify-center">
        <p class=" font-extrabold ">ویرایش پروفایل </p>
      </div>

      <div class=" w-full flex justify-between pr-40 desktop:pr-96">
        <input class="w-full"></input>
        <input class="w-full"></input>
        {/* <p class=" font-bold ">Edit Profile</p> */}
      </div>

      <div class="">
        <div class="w-full  ">
          <div class=" flex justify-center ">
            <img
              src={file}
              width={100}
              height={100}
              class=" rounded-xl mb-2 "
            />
          </div>

          <div class="flex justify-center">
            <label for="intFile">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-6 w-6 ml-2"
                viewBox="0 0 20 20"
                fill="currentColor"
              >
                <path
                  fillRule="evenodd"
                  d="M4 5a2 2 0 00-2 2v8a2 2 0 002 2h12a2 2 0 002-2V7a2 2 0 00-2-2h-1.586a1 1 0 01-.707-.293l-1.121-1.121A2 2 0 0011.172 3H8.828a2 2 0 00-1.414.586L6.293 4.707A1 1 0 015.586 5H4zm6 9a3 3 0 100-6 3 3 0 000 6z"
                  clipRule="evenodd"
                />
              </svg>
              <input
                type="file"
                id="intFile"
                class="w-10 h-10 hidden"
                onChange={(e) => handleFileInputChange(e)}
              />
            </label>
            <button
              class="w-6 h-6 rounded-full bg-black justify-center"
              onClick={submitAvatar}
            >
              <p class="font-semibold text-white ">+</p>
            </button>
          </div>
        </div>
      </div>
      <div class="">
        <div class="flex justify-around m-8 ">
          <input
            type="text"
            onChange={handleName}
            value={name}
            class="w-full px-1 bg-slate-100 shadow-xl shadow-gray-500/50 text-right placeholder:text-xs"
            placeholder="لطفا نام خود را وارد کنید"
          ></input>
          <div class="justify-center">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-6 w-6 ml-2"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fillRule="evenodd"
                d="M18.303,4.742l-1.454-1.455c-0.171-0.171-0.475-0.171-0.646,0l-3.061,3.064H2.019c-0.251,0-0.457,0.205-0.457,0.456v9.578c0,0.251,0.206,0.456,0.457,0.456h13.683c0.252,0,0.457-0.205,0.457-0.456V7.533l2.144-2.146C18.481,5.208,18.483,4.917,18.303,4.742 M15.258,15.929H2.476V7.263h9.754L9.695,9.792c-0.057,0.057-0.101,0.13-0.119,0.212L9.18,11.36h-3.98c-0.251,0-0.457,0.205-0.457,0.456c0,0.253,0.205,0.456,0.457,0.456h4.336c0.023,0,0.899,0.02,1.498-0.127c0.312-0.077,0.55-0.137,0.55-0.137c0.08-0.018,0.155-0.059,0.212-0.118l3.463-3.443V15.929z M11.241,11.156l-1.078,0.267l0.267-1.076l6.097-6.091l0.808,0.808L11.241,11.156z"
                clipRule="evenodd"
              />
            </svg>
          </div>
          
        </div>

        <div class="flex justify-center mx-8 mb-4">
        <textarea
            onChange={handleType}
            value={type}
            class="w-full p-2 bg-slate-100 shadow-xl shadow-gray-500/50 text-right placeholder:text-xs"
            maxLength={200}
            placeholder="چیزی از خودتان بنویسید"
          ></textarea>
          <div class="justify-center self-center  ">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-6 w-6 ml-2"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fillRule="evenodd"
                d="M17.283,5.549h-5.26V4.335c0-0.222-0.183-0.404-0.404-0.404H8.381c-0.222,0-0.404,0.182-0.404,0.404v1.214h-5.26c-0.223,0-0.405,0.182-0.405,0.405v9.71c0,0.223,0.182,0.405,0.405,0.405h14.566c0.223,0,0.404-0.183,0.404-0.405v-9.71C17.688,5.731,17.506,5.549,17.283,5.549 M8.786,4.74h2.428v0.809H8.786V4.74z M16.879,15.26H3.122v-4.046h5.665v1.201c0,0.223,0.182,0.404,0.405,0.404h1.618c0.222,0,0.405-0.182,0.405-0.404v-1.201h5.665V15.26z M9.595,9.583h0.81v2.428h-0.81V9.583zM16.879,10.405h-5.665V9.19c0-0.222-0.183-0.405-0.405-0.405H9.191c-0.223,0-0.405,0.183-0.405,0.405v1.215H3.122V6.358h13.757V10.405z"
                clipRule="evenodd"
              />
            </svg>
          </div>
         
        </div>
        <div class="flex justify-center">
          <button
            onClick={handleEdit}
            class="mb-8 h-12 px-8 py-2 bg-amber-200 grid justify-center rounded-lg border-2 border-cyan-800 hover:bg-white hover: "
          >
            <p class="text-center font-semibold ">ویرایش</p>
          </button>
        </div>
      </div>

    </div>
  );
};

export default EditProfile;
