import React, { useState, useRef } from "react"
import { Link, useNavigate } from "react-router-dom"
import { useParams } from "react-router"
import Cookies from "universal-cookie";
import axios from 'axios';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { toast } from "react-toastify";


const cookies = new Cookies();

export default function SubmitBlog() {
    // let { _id } = useParams();
    let navigate = useNavigate();


    const [title, setTitle] = useState("");
    const [imgurl, setImgUrl] = useState("");



    const handleimageFile = (e) => {
        setImgUrl(e.target.value);

    }

    const handleTitle = (e) => {
        setTitle(e.target.value);
    }

    //مربوط به  editor html

    const [content, setContentState] = useState(""
        //     EditorState.createEmpty()
    );
    const handleContentonchange = (e, editor) => {
        console.log(editor.getData());
        const data = editor.getData()
        setContentState(data)
    }



    //submit
    const token = cookies.get('token')

    const handleSubmitBlog = () => {
        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'auth': token ? `ut ${token}` : null
            },
            body: JSON.stringify({ title, content, imgurl }),
        };
        fetch('http://localhost:4000/blog/write', requestOptions)
            .then(response => response.json())
            .then(data => {

                if (data._id) {
                    toast.success("با موفقیت ثبت شد")
                    setTimeout(() => {
                        const target = `/blog/${data._id}`
                        navigate(target);
                    }, 2000);
                } else {
                    toast.warning("اطلاعات را کامل وارد کنید")
                }
            });


    }




    return (
        <div class="bg-white w-full h-screen">

            <p class="text-center font-extrabold pt-6">مقاله جدید</p>


            <div class="">
                <div class="m-8">
                    <div class="flex w-full">
                        <input type="text" class="px-1 w-full bg-slate-100 text-cyan-800 placeholder-gray-600 rounded-sm shadow-xl shadow-gray-500/50 text-right placeholder:text-xs" placeholder=" عنوان" onChange={handleTitle} ></input>
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 self-center ml-2" viewBox="0 0 20 20" fill="currentColor">
                            <path fillRule="evenodd" d="M17.728,4.419H2.273c-0.236,0-0.429,0.193-0.429,0.429v10.304c0,0.234,0.193,0.428,0.429,0.428h15.455c0.235,0,0.429-0.193,0.429-0.428V4.849C18.156,4.613,17.963,4.419,17.728,4.419 M17.298,14.721H2.702V9.57h14.596V14.721zM17.298,8.712H2.702V7.424h14.596V8.712z M17.298,6.566H2.702V5.278h14.596V6.566z M9.142,13.005c0,0.235-0.193,0.43-0.43,0.43H4.419c-0.236,0-0.429-0.194-0.429-0.43c0-0.236,0.193-0.429,0.429-0.429h4.292C8.948,12.576,9.142,12.769,9.142,13.005" clipRule="evenodd" />
                        </svg>
                    </div>
                </div>

                <div class="m-8">
                    <div class="flex w-full ">
                        <input type="text" class="px-1 w-full bg-slate-100 text-cyan-800 placeholder-gray-600 rounded-sm shadow-xl shadow-gray-500/50 text-right placeholder:text-xs" placeholder="لطفا آدرس عکس مورد نظر را اینجا قرار دهید " onChange={handleimageFile}></input>

                        <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 self-center ml-2" viewBox="0 0 20 20" fill="currentColor">
                            <path fillRule="evenodd" d="M18.555,15.354V4.592c0-0.248-0.202-0.451-0.45-0.451H1.888c-0.248,0-0.451,0.203-0.451,0.451v10.808c0,0.559,0.751,0.451,0.451,0.451h16.217h0.005C18.793,15.851,18.478,14.814,18.555,15.354 M2.8,14.949l4.944-6.464l4.144,5.419c0.003,0.003,0.003,0.003,0.003,0.005l0.797,1.04H2.8z M13.822,14.949l-1.006-1.317l1.689-2.218l2.688,3.535H13.822z M17.654,14.064l-2.791-3.666c-0.181-0.237-0.535-0.237-0.716,0l-1.899,2.493l-4.146-5.42c-0.18-0.237-0.536-0.237-0.716,0l-5.047,6.598V5.042h15.316V14.064z M12.474,6.393c-0.869,0-1.577,0.707-1.577,1.576s0.708,1.576,1.577,1.576s1.577-0.707,1.577-1.576S13.343,6.393,12.474,6.393 M12.474,8.645c-0.371,0-0.676-0.304-0.676-0.676s0.305-0.676,0.676-0.676c0.372,0,0.676,0.304,0.676,0.676S12.846,8.645,12.474,8.645" clipRule="evenodd" />
                        </svg>
                    </div>
                </div>

            </div>
            <div class="flex justify-center m-2 scroll-my-12 snap-start">
                <div class="bg-orange-200 w-full h-full m-1 ">
                    <CKEditor
                        editor={ClassicEditor}
                        onChange={handleContentonchange}
                    />

                </div>
            </div>
            <div class="flex justify-center m-5">
                <button onClick={handleSubmitBlog} class="self-center h-12 px-8 py-1 rounded-xl border-2 border-cyan-800 bg-white hover:bg-amber-200">
                    <p class="font-semibold">ارسال</p>
                </button>
            </div>

        </div>
    )
}
