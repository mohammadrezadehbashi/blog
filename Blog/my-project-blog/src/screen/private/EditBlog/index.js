import React, { useEffect, useState } from "react"
import { Link, useNavigate, useParams } from "react-router-dom"
import Cookies from "universal-cookie"
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { toast } from "react-toastify";

const cookies = new Cookies();

const EditBlog = () => {

    let navigate = useNavigate();
    const parse = require('html-react-parser');
    const { _id } = useParams();


    const [blogId, setBlogId] = useState(null);
    const [title, setTitle] = useState("");
    // const [content, setContent] = useState("");
    const [imgurl, setImgurl] = useState("");
    //مربوط به  editor html
    const [content, setContent] = useState("");

    const handleContentonchange = (e, editor) => {
        console.log(editor.getData());
        const data = editor.getData()
        setContent(data)
    }
    const handleTitle = (e) => {
        setTitle(e.target.value);
    }
    // const handleContent = (e) => {
    //     setContent(e.target.value);
    // }
    const handleImg = (e) => {
        setImgurl(e.target.value);
    }

    useEffect(() => {
        getBlog();
    }, [])

    const getBlog = async () => {
        try {
            const response = await fetch(`http://localhost:4000/blog/single-blog/${_id}`)
                .then(response => response.json())
                .then(data => {
                    if (data) {
                        setTitle(data.title);
                        setContent(data.content);
                        setImgurl(data.imgurl);
                        setBlogId(data);

                    }
                    setBlogId(data);
                })
        } catch (error) {
            console.error(error);
        }
    };
   



    const editBlogRequest = () => {
        console.log("idididid" +blogId)

        const token = cookies.get('token')
        console.log("values :", blogId, title, content, imgurl)
        fetch("http://localhost:4000/blog/edit", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'auth': token ? `ut ${token}` : null
            },
            body: JSON.stringify({
                blogId: _id,//this declare is required
                data: {
                    title: title
                    , content: content
                    , imgurl: imgurl
                    //this declare is not required
                }
            })
        })
            .then(response => response.json())
            .then(data => {
                console.warn(data);
                // const target = `../blog/${data._id}`
                // navigate(target);
                toast("ویرایش انجام شد")
                setTimeout(() => {
                    const target = `/blog/${_id}`
                    navigate(target);
                }, 2000);
                
            }
            )
    }


    if (!blogId || !blogId._id) return <h1>loading</h1>

    return (
        <div class="bg-white w-full h-screen">
            <p class="text-center font-extrabold pt-6"> ویرایش مقاله</p>

            <div class=" ">
                <div class="m-8 ml-64">
                    <input type="text" class=" w-fit bg-slate-100 shadow-xl shadow-gray-500/50 text-right placeholder:text-xs" placeholder="عنوان جدید" value={title} onChange={handleTitle} id="title" />
                    <lable class="font-medium text-slate-900" for="title">{" "} : ویرایش عنوان</lable>

                </div>
                <div class="flex justify-between h-16 mx-16">
                <div class="self-center -mr-0 lg:-mr-10 semi-lg:-mr-5 xs:mr-0 bg-cyan-50 h-16 w-16">
                        <img src={blogId.imgurl.substr(0, 4)==="http"?blogId.imgurl:"/assets/images/icons8-image-100.png"} class="w-full h-full"/>
                    </div>
                    <div class=" mt-4 lg:-ml-10 semi-lg:-ml-5 xs:ml-0">
                        <input type="text" class="px-1 h-8 bg-slate-100 shadow-xl shadow-gray-500/50 max-w-full xs:w-24 text-right placeholder:text-xs" onChange={handleImg} value={imgurl} placeholder="آدرس عکس را اینجا قرار دهید" id="image" />
                        <lable class="font-medium text-slate-900" for="image">{" "} : ویرایش عکس</lable>
                    </div>
                    
                </div>
            </div>
            <div class="flex justify-center m-4 ">

                <div class="w-full">
                    <CKEditor
                        editor={ClassicEditor}
                        data={content}
                        onChange={handleContentonchange}

                    />
                </div>

            </div>
            <div class="flex justify-center item-center ">
                <div class="items-center h-12 px-8 py-2 rounded-xl border-2 border-cyan-800 bg-amber-200 hover:bg-white" 
                onClick={editBlogRequest} >                  
                        <p class="font-semibold text-center">ویرایش</p>
                </div>
            </div>
        </div>
    )
}

export default EditBlog