import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Cookies from "universal-cookie";
import parse from "html-react-parser";
import DeleteBlog from "./DeleteBlog";

const cookies = new Cookies();

const BlogsMe = () => {
  const [loading, setLoading] = useState(true);
  const [list, setList] = useState([]);

  const token = cookies.get("token");
  const parse = require("html-react-parser");

  const getBlogMe = () => {
    try {
      fetch("http://localhost:4000/blog/my-blogs", {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          auth: token ? `ut ${token}` : null,
        },
      })
        .then((response) => response.json())
        .then((data) => {
          setList(data);
        });
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    getBlogMe();
  }, []);
console.log("list..",list);
  if (loading) return <h1>loading</h1>;

  return (
    // <div class="">
    <div class="justify-between w-full  m-6 border-l-8 border-cyan-800 ">
      {list.length !== 0 ? (
        list.map((item) => {
          return (
            <div class="flex justify-between w-full mb-3 py-2 bg-white semi-semi-lg:h-28 xs:rounded-3xl">
              <div class="gap-4">
                <div class="w-32 h-32  m-1 -ml-5 rounded-xl semi-semi-lg:h-20 semi-semi-lg:w-20 ">
                  {/* <p class="text-white">{item.imgurl}</p> */}
                  <img
                    src={item.imgurl.substr(0, 4) === "http"||item.imgurl.substr(0,4)==="data"  ? item.imgurl : '/assets/images/icons8-image-100.png'}
                    class="bg-white w-full h-full shadow-2xl shadow-current xs:rounded-3xl"
                  />
                </div>
              </div>

              <div class=" w-full">
                <Link to={`/blog/${item._id}`}>
                  <div class="mt-2 mb-1 mx-6 semi-semi-lg:mt-0.5 xs:mx-1">
                    <p class="font-bold xs:text-xs">{item.title}</p>
                  </div>
                </Link>
                <div class="overflow-hidden h-12 mx-6 semi-semi-lg:mx-3 ">
                  <p class="text-justify font-medium text-stone-500 semi-semi-lg:text-xs xs:hidden">
                    {parse(item.content)}
                  </p>
                </div>

                <div class="flex items-stretch  semi-semi-lg:mx-10 semi-semi-lg:-my-3">
                  <Link
                    class="grid self-center w-full px-8 py-1 rounded-md mx-2 border border-amber-200
                bg-gradient-to-r from-cyan-800  hover:bg-gradient-to-l hover:from-amber-200
                semi-semi-lg:px-1 semi-semi-lg:mx-0 semi-semi-lg:h-8"
                    to={`/Dashboard/blog/edit/${item._id}`}
                  >
                    <button class=" justify-self-center">
                      <h5 class="text-cyan-800 ">ویرایش</h5>
                    </button>
                  </Link>
                  <div class="grid self-center w-full px-8 py-1 rounded-md mx-2 border border-red-200
                bg-gradient-to-r from-cyan-800  hover:bg-gradient-to-l hover:from-red-200
                semi-semi-lg:px-1 semi-semi-lg:mx-0 semi-semi-lg:h-8">
                <DeleteBlog blogId={item._id}  getBlogMe={getBlogMe}/>
                </div>
                </div>
              </div>
            </div>
          );
        })
      ) : (
        <div class=" w-full mb-3 py-2 bg-white h-screen ">
             <img src="/assets/images/icons8-nothing-found-80.png" width={100} height={100} class="mx-auto mt-44"/>
          <p class="text-center mt-16">هیچ مقاله ای از شما در سایت ثبت نشده است</p>
        </div>
      )}
    </div>
    // </div>
  );
};

export default BlogsMe;
