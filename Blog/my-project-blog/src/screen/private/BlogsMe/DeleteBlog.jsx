import { toast } from "react-toastify";
import Cookies from "universal-cookie";

const coockies=new Cookies();

function DeleteBlog({blogId,getBlogMe}) {

    console.log("diddid",blogId);
    const token=coockies.get('token')

const deleteHandle=()=>{
console.log("tokentoken",token);

    fetch("http://localhost:4000/blog/delete",{
        method:"POST",
        headers: {
            "Content-Type": "application/json",
          auth: token ? `ut ${token}` : null,
          },
          body:JSON.stringify({blogId:blogId})
    })
    .then(response=>response.json())
    .then(data=>{
        toast("مقاله ی مورد نظر حذف شد")
        setTimeout(()=>{
            getBlogMe()
        },2000)
    })
}

    return (
        <>
            <div onClick={deleteHandle}>حذف مقاله</div>
        </>
    );
}

export default DeleteBlog;