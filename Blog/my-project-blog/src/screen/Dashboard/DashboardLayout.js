import { useEffect, useState } from "react";
import { Link, Outlet, useParams } from "react-router-dom"
import Cookies from "universal-cookie";
import Calenderr from "../component/Calenderr";
import { ToastContainer } from "react-toastify";
import "react-toastify/ReactToastify.min.css";

const cookies = new Cookies();

function DashboardLayout() {

  const [toggle, setToggle] = useState(true);
  const [name, setName] = useState(true);

  const handleCheeckbox = () => {
    setToggle(!toggle);
  }


  const [loading, setLoading] = useState(true)

  useEffect(() => {
    //حالا توکن را که در لاگین داده بودیم باید به بک اند بدیم یا بشناسونیم... برو توی هدر
    const token = cookies.get('token')

    console.log('token:', token)

    fetch("http://localhost:4000/user/me", {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'auth': token ? `ut ${token}` : null
        // خط بالا قرار دادی هست بین فرانت و بک که باید از بک گرفت و در واقع تحویل دادن توکن به بکه برای اجازه اتورایز شدن حالا در داک بکند ریکوایراوث اگر ترو بود خط بالا باید باشه در هر فتچ
      },
      body: JSON.stringify({})
    })
      .then(response => response.json())
      .then(data => {
        console.log('data',data)
        if (data._id) {
          setName(data.name)
          setLoading(false)
        }
      })

  }, []);

  if (loading) return(
    <div class="text-center ">
    <img src="/assets/images/icons8-fingerprint-scan-100.png" width={160} height={160} class="mx-auto mt-56"/>
    <h1 className="text-2xl text-slate-800 font-bold"> ابتدا وارد سامانه شوید </h1>
    </div>
    )
    

  return (
    // <>
    <div class="flex relative">
      <div class=" w-1/4 h-screen semi-semi-lg:bg-gradient-to-r semi-semi-lg:to-bg-white semi-semi-lg:from-cyan-800">
        <img src={'/assets/images/10531425205221697842.webp'} class="h-full w-full semi-semi-lg:hidden" />
      </div>

      <div class=" w-3/4 blur-md bg-gradient-to-l from-cyan-300 to-cyan-800 semi-semi-lg:to-white  semi-semi-lg:from-amber-200 semi-semi-lg:blur-lg"></div>

      {/* --content in absolute page-- */}
      <div
        class="grid grid-cols-5 gap-4 absolute backdrop-blur-sm bg-white/20  w-5/6 h-5/6 ml-1/6 rounded-lg shadow-sm shadow-white-500/50
       semi-semi-lg:grid-cols-1 semi-semi-lg:grid-row-1 semi-semi-lg:backdrop-blur-sm semi-semi-lg:bg-black/40"
        style={{ right: "8.5%", left: "8.5%", top: "8%", bottom: "8%" }}
      >
        {/* overflow-hidden */}
        <div
          class=" mobile:col-start-1 mobile:col-end-4  mobile:flex absolute semi-semi-lg:col-end-6 semi-semi-lg:hidden"
          style={{ top: "0.1%", bottom: "0.1%" }}
        >
          {/* // overflow-hidden relative */}

          {/* --toggle for opening and closing checkbox-- */}
          {toggle ? (
            <input
              type="checkbox"
              class="peer absolute cursor-pointer top-0 z-10 h-16 w-16  opacity-0"
              onClick={handleCheeckbox}
              style={{ left: "13%" }}
            ></input>
          ) : (
            <input
              type="checkbox"
              class="peer absolute cursor-pointer top-0 z-10 h-16 w-16 opacity-0"
              onClick={handleCheeckbox}
              style={{ left: "35%" }}
            ></input>
          )}
          {/* items route */}
          <div
            class=" grid duration-500 peer-checked:-transition-all w-1/6 peer-checked:w-3/6  peer-checked:-translate-x-5 semi-semi-lg:flex"
            style={{ alignItems: "center" }}
          >
            {/* peer-checked:overflow-hidden */}
            {[
              [
                "Home",
                "/",
                "M10.707 2.293a1 1 0 00-1.414 0l-7 7a1 1 0 001.414 1.414L4 10.414V17a1 1 0 001 1h2a1 1 0 001-1v-2a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 001 1h2a1 1 0 001-1v-6.586l.293.293a1 1 0 001.414-1.414l-7-7z",
              ],
              [
                "Profile",
                "/Dashboard/profile/edit",
                "M10 9a3 3 0 100-6 3 3 0 000 6zm-7 9a7 7 0 1114 0H3z",
              ],
              [
                "New Blog",
                "/Dashboard/blog/submit",
                "M3 4a1 1 0 011-1h12a1 1 0 011 1v2a1 1 0 01-1 1H4a1 1 0 01-1-1V4zM3 10a1 1 0 011-1h6a1 1 0 011 1v6a1 1 0 01-1 1H4a1 1 0 01-1-1v-6zM14 9a1 1 0 00-1 1v6a1 1 0 001 1h2a1 1 0 001-1v-6a1 1 0 00-1-1h-2z",
              ],
              [
                "My Blog",
                "/Dashboard/myblogs",
                "M9 2a2 2 0 00-2 2v8a2 2 0 002 2h6a2 2 0 002-2V6.414A2 2 0 0016.414 5L14 2.586A2 2 0 0012.586 2H9z",
              ],
            ].map(([title, url, d]) => (
              <Link class="flex overflow-hidden justify-between " to={url}>
                {/* <p class="text-start ml-6 mr-16">icon</p> */}
                <div class="ml-5 mr-12 self-center lg:mr-4 ">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className=" w-10 h-10 semi-lg:h-7 semi-lg:w-6 hover:text-sky-800"
                    viewBox="0 0 20 20"
                    fill="currentColor"
                  >
                    <path d={d} />
                  </svg>
                </div>
                <div class=" self-center mr-2 lg:mr-3 ">
                  <p class="font-bold w-24 text-black lg:text-sm semi-semi-lg:hidden">
                    {title}
                  </p>
                </div>
              </Link>
            ))}
          </div>

          {/* --icon-- */}
          <div class="absoulote transition-transform duration-500 rotate-0 peer-checked:rotate-180 h-6 w-3 text-white pr-8">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              class="h-6 w-6"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
              stroke-width="2"
            >
              <path
                stroke-linecap="round"
                stroke-linejoin="round"
                d="M9 5l7 7-7 7"
              />
            </svg>
          </div>
          {/* --card left inside-- */}

          <div
            class=" semi-semi-lg:w-full semi-semi-lg:bg-cyan-800 semi-semi-lg:my-4 semi-semi-lg:mx-1 mosemi-semi-lgbile:rounded-xl semi-semi-lg:h-11/12 "
            style={{ overflow: "scroll", width: "40vw" }}
          >
            <Outlet />
          </div>
        </div>
        {/* semi-semi-lg:hidden */}
        {/* //////////////////////////////semi-semi-lg قسمت دوم */}
        <div
          class=" semi-semi-lg:h-10 semi-semi-lg:col-start-1 semi-semi-lg:col-end-4  semi-semi-lg:flex semi-semi-lg:justify-self-center absolute  mobile:hidden"
          style={{ top: "2%", bottom: "0.1%" }}
        >
          <div class=" semi-semi-lg:flex ">
            {[
              [
                "/",
                "M10.707 2.293a1 1 0 00-1.414 0l-7 7a1 1 0 001.414 1.414L4 10.414V17a1 1 0 001 1h2a1 1 0 001-1v-2a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 001 1h2a1 1 0 001-1v-6.586l.293.293a1 1 0 001.414-1.414l-7-7z",
              ],
              [
                "/Dashboard/profile/edit",
                "M10 9a3 3 0 100-6 3 3 0 000 6zm-7 9a7 7 0 1114 0H3z",
              ],
              [
                "/Dashboard/blog/submit",
                "M3 4a1 1 0 011-1h12a1 1 0 011 1v2a1 1 0 01-1 1H4a1 1 0 01-1-1V4zM3 10a1 1 0 011-1h6a1 1 0 011 1v6a1 1 0 01-1 1H4a1 1 0 01-1-1v-6zM14 9a1 1 0 00-1 1v6a1 1 0 001 1h2a1 1 0 001-1v-6a1 1 0 00-1-1h-2z",
              ],
              [
                "/Dashboard/myblogs",
                "M4 4a2 2 0 012-2h4.586A2 2 0 0112 2.586L15.414 6A2 2 0 0116 7.414V16a2 2 0 01-2 2H6a2 2 0 01-2-2V4z",
              ],
            ].map(([url, d]) => (
              <Link class="w-full" to={url}>
                {/* <p class="text-start ml-6 mr-16">icon</p> */}
                <div class="ml-5 mr-12 self-center lg:mr-4 ">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className=" semi-semi-lg:w-6 semi-semi-lg:h-6 justify-self-center"
                    viewBox="0 0 20 20"
                    fill="currentColor"
                  >
                    <path d={d} />
                  </svg>
                </div>
              </Link>
            ))}
          </div>
        </div>
        <div
          class="semi-semi-lg:w-auto semi-semi-lg:bg-cyan-800 semi-semi-lg:mb-5 semi-semi-lg:mt-10 semi-semi-lg:mx-1 semi-semi-lg:rounded-xl semi-semi-lg-mobile:hidden"
          style={{ overflowY: "scroll" }}
        >
          <Outlet class="semi-semi-lg:w-full" />
        </div>
        {/* //////////////////////// */}
        {/* --card right-- */}
        <div
          class="col-span-2 col-start-4 col-end-6 grid grid-rows-5 absolute semi-semi-lg:hidden"
          style={{ top: "0.1%", bottom: "0.1%" }}
        >
          <div class="row-start-1 row-span-1 my-4 mr-4 ml-1 rounded-xl bg-amber-200 flex justify-between items-center ">
            <div class="grid place-content-center m-2">
              <p>{name}</p>
            </div>

            <div class="w-20 h-12 m-2 bg-black rounded-xl">
              <p class="text-white text-center">image</p>
            </div>
          </div>

          <div class="row-start-2 row-end-6 bg-amber-200 mr-4 mb-4 ml-2 rounded-lg h-11/12 opacity-60">
            <Calenderr />
          </div>
        </div>
      </div>
      <ToastContainer
        position="top-right"
        autoClose={2000}
        hideProgressBar={false}
        newestOnTop
        closeOnClick
        rtl={true}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="dark"
      />
    </div>
    // </>
  );
}



export default DashboardLayout;
