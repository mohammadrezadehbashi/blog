import React, { useEffect, useState } from "react"
import { Link } from "react-router-dom"


const TopBlogs = () => {

    const [loading, setLoading] = useState(true);
    const [list, setList] = useState([]);

    useEffect(() => {
        getBlog();
    }, [])
    
    const getBlog = () => {
        try {
            fetch("http://localhost:4000/blog/top-blogs")
                .then(response => response.json())
                .then((data) => {
                  setList(data);
            
                    setLoading(false);

                }
                )
        } catch (error) {
            console.error(error);
        } 
    }


    if (loading) return <h1>loading</h1>
    return (
        <>
            <div>
                <div class="mt-4">
                    <p class="text-center">Blogs</p>
                </div>

                {list.map((item) => {
                    return (
                        <div class="grid grid-cols-2 bg-sky-100 overflow-hidden m-2 border-l-4 border-rose-600">

                            <div class="flex items-stretch ml-2 ">
                                <Link class="self-center h-12 px-8 py-1 rounded-xl bg-slate-600" to={`/blog/${item._id}`}>
                                    <button class="">
                                        <p>view</p>
                                    </button>
                                </Link>
                            </div>

                            <div class=" grid grid-cols-4" >
                                <div class="mt-2 col-start-2">
                                    <Link to="">
                                        <p class="">{item.title}</p>
                                    </Link>
                                </div>
                                <div class=" relative col-start-3 col-end-5 h-32  self-start rounded-md bg-black">
                                    <p class="text-white">{item.imgurl}</p>
                                    <div class="absolute bg-white p-4 w-14 rounded-full bottom-3" style={{ right: "91%" }}>
                                        <p class="text-center ">avatar</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    )
                }

                )}


              


            </div>
        </>
    )
}

export default TopBlogs