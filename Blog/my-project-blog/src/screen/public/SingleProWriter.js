import React, { useEffect, useState } from "react";
import { useParams } from "react-router";
import Cookies from "universal-cookie";
import { useAuth } from "../../Context";
import { Link } from "react-router-dom";

const cookies = new Cookies();

const SingleProWriter = (props) => {
  const { _id } = useParams();
  // const {type}=useParams()
  const [writer, setwriter] = useState(null);

  const [list, setList] = useState([]);

  const getUser = async () => {
    try {
      const response = await fetch(
        `http://localhost:4000/user/singleUser/${_id}`
      )
        .then((response) => response.json())
        .then((data) => {
          if (data._id) {
            setwriter(data);
            console.log("hi", data._id);
          }
        });
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    getUser();
    PostByUser();
  }, []);

  console.log("blogs" + writer);
  if (!writer || !writer._id) return;
  <h1>loading</h1>;

  //blog/by-user ---two

  function PostByUser() {
    console.log("-id", _id);

    fetch("http://localhost:4000/blog/by-user", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ _id }),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log("-data", data);

        setList(data);

      });
  }


  return (
    <>
      <div
        class=" p-40 bg-[url('/src/assets/images/toy-bricks-table-with-word-blog.jpg')] h-screen bg-cover 
            xl:p-20 md:p-8 semi-md:p-16"
      >
        <div class="absolute right-10 top-10 semi-md:right-5 semi-md:top-5">
          <Link to="/">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className=" w-10 h-10 text-white semi-lg:h-7 semi-lg:w-6"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                d={
                  "M10.707 2.293a1 1 0 00-1.414 0l-7 7a1 1 0 001.414 1.414L4 10.414V17a1 1 0 001 1h2a1 1 0 001-1v-2a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 001 1h2a1 1 0 001-1v-6.586l.293.293a1 1 0 001.414-1.414l-7-7z"
                }
              />
            </svg>
          </Link>
        </div>
        <div class="flex flex-row sm:flex-none">
          <div class="basic-1/2 w-60 h-60 md:basic-1/4 sm:basic-2/6 semi-sm:basic-1/6">
            <img
              src={'./assets/images/The Mask wallpaperz.blog.ir .jpeg'}
              class=" w-full h-full rounded-xl semi-sm:h-40 semi-md:h-36 semi-md:w-48"
            />
          </div>
          <div class="basic-1/2 w-60 overflow-hidden md:w-96 sm:basic-4/6 semi-md:h-36 semi-sm:basic-5/6 semi-sm:h-40">
            <div class="flex ">
              <div class="w-3 h-3 mx-3 self-center bg-rose-700 rounded-full"></div>
              <p class="ml-2 text-rose-500 text-xl font-bold semi-sm:text-sm">
                {writer.name}
              </p>
            </div>
            <div class="flex">
              <div class="w-2 h-2 ml-3.5 mr-0 indent-8 self-center bg-rose-700 rounded-full"></div>
              <p class="ml-2 font-semibold text-white semi-sm:text-xs">
                {writer.bio}
              </p>
            </div>
          </div>
        </div>

        <div class="flex justify-center mt-10 h-40 lg:mt-4 semi-lg:h-32 semi-md:h-24 semi-md:-mt-10 up-semi-sm:h-12 semi-sm:h-8">
          {list.map((item) => (
            <Link to={`/blog/${item._id}`} class="">
              <div class="my-1 mx-2 h-full w-32 border-2 border-rose-500  rounded-lg lg:w-24 semi-md:w-16 up-semi-sm:w-12 semi-sm:mx-0.5">
                <img src={item.imgurl.substr(0,4)==="http"||item.imgurl.substr(0,4)==="data" ?item.imgurl:'/assets/images/icons8-image-100.png'} class="h-full semi-sm:w-full" />
              </div>
              <div class=" mx-3 -mt-40 semi-md:-mt-24 up-semi-sm:-mt-12">
                <p class="font-semibold text-rose-500 lg:text-xs semi-md:text-[0.52rem] up-semi-sm:text-[0.32rem] semi-sm:hidden">
                  {item.title}
                </p>
              </div>
            </Link>
          ))}
        </div>
      </div>

    </>
  );
};

export default SingleProWriter;

