import React, { useEffect, useState } from "react"
import { Link } from "react-router-dom"


const ListBlogs = () => {

    const [loading, setLoading] = useState(true);
    const [list, setList] = useState([]);

    useEffect(() => {
        getBlog();
    }, [])

    const getBlog = () => {
        try {
            fetch("http://localhost:4000/blog")
                .then(response => response.json())
                .then((data) => {
                    setList(data);
                    setLoading(false);
                }
                )
        } catch (error) {
            console.error(error);
        }
        // finally {
        //     setLoading(false);
        // }
    }



    if (loading) return (<div class=" min-h-screen">
        <p>...loading</p></div>
    )

    return (
        <div class=" min-h-screen">
            <div>
                <p class="text-right p-14 font-serif font-bold text-2xl">لیست مقالات</p>

                {list.length !== 0 ? list.map((item) => {
                    return (
                        <div class="flex justify-between m-4 px-16 py-2 rounded-md bg-sky-300 border-2 border-red-900
                        sm:px-6 semi-sm:px-1 xs:m-1">

                            <div class="grid bg-rose-300 w-16 h-16 rounded-full semi-sm:w-12 semi-sm:h-12 semi-sm:my-2">
                                <Link to={`/blog/${item._id}`} class="flex justify-center  bg-rose-400 m-1 rounded-full">
                                    <p class="self-center font-medium text-rose-900">نمایه</p>
                                </Link>
                            </div>
                            <div class="flex justify-between w-72  sm:w-56 semi-sm:w-44">
                                <div class="flex first-letter:bg-blue-400 w-20 h-20  rounded-xl overflow-hidden 
                                semi-sm:w-10 semi-sm:h-10 semi-sm:my-3">
                                    <img src={item.imgurl.substr(0, 4) === "http"||item.imgurl.substr(0,4)==="data"  ? item.imgurl : '/assets/images/icons8-image-100.png'} class="self-center h-full w-full" />
                                </div>
                                <div class="flex order-first mr-4 ">
                                    <p class="text-xl font-semibold text-sky-900 self-center semi-sm:text-base ">{item.title}</p>
                                </div>
                            </div>
                        </div>
                    )
                }
                )
                    :
                    <div class=" w-full mb-3 py-2 h-screen ">
                        <img src="/assets/images/icons8-nothing-found-80.png" width={100} height={100} class="mx-auto mt-20" />
                        <p class="text-center mt-16">هیچ مقاله ای  در سایت ثبت نشده است</p>
                    </div>}

            </div>
        </div>
    )
}

export default ListBlogs