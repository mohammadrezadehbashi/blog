import React, { useState } from "react"
import Cookies from 'universal-cookie';
import { Link, useNavigate } from "react-router-dom";
import { toast,ToastContainer } from "react-toastify";


const cookies = new Cookies();

const Login = () => {

    let navigate = useNavigate();

    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    const handleUserName = (e) => {
        setUsername(e.target.value);
    }
    const handlePassword = (e) => {
        setPassword(e.target.value);
    }

     const handleSubmit = async () => {
       let item = { username, password:'1111'};

       let result = await fetch("http://localhost:4000/user/login", {
         method: "POST",
         headers: {
           "Content-Type": "application/json",
         },
         body: JSON.stringify(item),
       });

       result = await result.json();
       if (result.msg==='bad request: no such user exists') {
        toast.error("نام کاربری اشتباه است ")
       } else {
        cookies.set("token", result.token);
       console.log(cookies.get("token"));

       navigate("/Dashboard/profile/edit", { replace: true });
       }
       
       // window.location.href='./'
     };

    // const cookies = new Cookies();
    //     const handleSubmit  = async event => {
    //         event.preventDefault();
    //         const { username, password } = this.state;
    //         const response = await login(username, password);
    //         cookies.set("access_token", response.data.token);
    //      };

    return (
      <div class="flex justify-center semi-sm:bg-sky-300">
        <div class=" w-96 h-96 my-24 bg-sky-300 rounded-md lg:my-16 semi-lg:my-12 md:my-9 sm:my-5">
          <div class="m-2 semi-md:right-5 semi-md:top-5 ">
            <Link to="/">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className=" w-6 h-6 text-white semi-lg:h-7 semi-lg:w-6"
                viewBox="0 0 20 20"
                fill="currentColor"
              >
                <path
                  d={
                    "M10.707 2.293a1 1 0 00-1.414 0l-7 7a1 1 0 001.414 1.414L4 10.414V17a1 1 0 001 1h2a1 1 0 001-1v-2a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 001 1h2a1 1 0 001-1v-6.586l.293.293a1 1 0 001.414-1.414l-7-7z"
                  }
                />
              </svg>
            </Link>
          </div>

          <div class="mt-14 mb-24">
            <p class="text-center text-lg font-bold text-sky-900">ورود</p>
          </div>

          <div class="flex flex-col mt-16 gap-4">
            <input
              type="text"
              placeholder="نام کاربری را وارد نمایید"
              onChange={handleUserName}
              class="mx-2 pl-2 pt-2 rounded-xl ring-1 ring-sky-400 text-right placeholder:text-right pr-3 pb-2"
            ></input>

            <input
              type="password"
              placeholder="گذرواژه را وارد نمایید"
              onChange={handlePassword}
              class="mx-2 pl-2 pt-2 rounded-xl ring-1 ring-sky-400 text-right placeholder:text-right pr-3 pb-2"
            ></input>

            <button
              type="submit"
              onClick={handleSubmit}
              class=" h-7 px-5 mx-auto bg-green-400 rounded-lg ring-2 ring-white 
                    hover:bg-white hover:text-green-400"
            >
              <p class="text-center text-lg font-semibold">ارسال</p>
            </button>
          </div>
        </div>
        <ToastContainer
        position="top-right"
        autoClose={2000}
        hideProgressBar={false}
        newestOnTop
        closeOnClick
        rtl={true}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="dark"
      />
      </div>
    );
}

export default Login