import React, { useState } from "react"
import Cookies from 'universal-cookie';
import { Link, useNavigate } from "react-router-dom";
import { toast } from "react-toastify";

const cookies = new Cookies();

const SignUp = () => {

    let navigate = useNavigate();


    const [username, setUserName] = useState("");
    const [name, setName] = useState("");

    const handleUserName = (e) => {
        setUserName(e.target.value);
    }

    const handleName = (e) => {
        setName(e.target.value);
    }


    const handleSubmit = () => {
        // e.preventDefault();
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ username, name })
        };
        fetch('http://localhost:4000/user/signup', requestOptions)
            .then(response => response.json())
            .then(data => {
                if (data.token) {
                    cookies.set('token', data.token)
                    console.log(cookies.get('token'));
                    //   return  window.location.href = '../'
                    return navigate("../../", { replace: true });
                }
            });

    }


    return (
      <div class="flex justify-center  semi-sm:bg-rose-400">
        <div class=" w-96 h-96 my-24 bg-rose-400 rounded-md lg:my-16 semi-lg:my-12 md:my-9 sm:my-5">
          <div class="m-2 semi-md:right-5 semi-md:top-5 ">
            <Link to="/">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className=" w-6 h-6 text-white semi-lg:h-7 semi-lg:w-6"
                viewBox="0 0 20 20"
                fill="currentColor"
              >
                <path
                  d={
                    "M10.707 2.293a1 1 0 00-1.414 0l-7 7a1 1 0 001.414 1.414L4 10.414V17a1 1 0 001 1h2a1 1 0 001-1v-2a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 001 1h2a1 1 0 001-1v-6.586l.293.293a1 1 0 001.414-1.414l-7-7z"
                  }
                />
              </svg>
            </Link>
          </div>

          <div class="my-20">
            <p class="text-center text-lg font-bold text-rose-900">ثبت نام</p>
          </div>

          <div class="flex flex-col mt-16 gap-4">
            <input
              type="text"
              value={username}
              placeholder="نام کاربری را وارد نمایید"
              onChange={handleUserName}
              class="mx-2 pl-2 pt-2 rounded-xl ring-1 ring-rose-500 text-right placeholder:text-right pr-3 pb-2"
            ></input>

            <input
              type="text"
              value={name}
              placeholder="نام را وارد نمایید"
              onChange={handleName}
              class="mx-2 pl-2 pt-2 rounded-xl ring-1 ring-rose-500 text-right placeholder:text-right pr-3 pb-2"
            ></input>

            <button
              type="submit"
              onClick={handleSubmit}
              class=" h-7 px-5 mx-auto bg-sky-700 rounded-lg ring-2 ring-white
                        hover:bg-white hover:text-blue-600"
            >
              <p class="text-center  text-lg font-semibold">ارسال</p>
            </button>
          </div>
        </div>
      </div>
    );
}

export default SignUp