import React, { useEffect, useState } from "react"
import { Link, useParams } from "react-router-dom"


const TopWriters = () => {
    const [loading, setLoading] = useState(true);
    const [list, setList] = useState([]);
    // let { _id } = useParams();
    const getBlog = () => {
        try {
            fetch("http://localhost:4000/user/top-users")
                .then(response => response.json())
                .then((data) => {
                    setList(data);
                 
                }
                )
        } catch (error) {
            console.error(error);
        } finally {
            setLoading(false);
        }
    }

    useEffect(() => {
        getBlog();
    }, [])

    console.warn("res", data)


    return (
        <>
            <p class="text-center m-4"> ListWriters</p>


            {
                list.map((item) =>
                    <>
                        <div class="grid grid-cols-2 bg-sky-100 overflow-hidden m-2 border-l-4 border-rose-600" >

                            <div class="flex items-stretch ml-2 ">
                                <Link class="self-center h-12 px-8 py-1 rounded-xl bg-slate-600" to={`/user/${item._id}`}>
                                    <button class="">
                                        <p>view</p>
                                    </button>
                                </Link>
                            </div>

                            <div class=" grid grid-cols-4" >
                                <div class="mt-2 col-start-2">
                                    <Link to="/user/:_id">
                                        <p class="" >{item.username}</p>
                                    </Link>
                                </div>
                                <div class=" relative col-start-3 col-end-5 h-32  self-start rounded-md bg-black">
                                    <p class="text-white">{item.name}</p>
                                    <Link to="/user/:_id">
                                        <div class="absolute bg-white p-4 w-14 rounded-full bottom-3" style={{ right: "91%" }}>
                                            <p class="text-center ">avatar</p>
                                        </div>
                                    </Link>
                                </div>
                            </div>
                        </div>

                    </>
                )
            }



        </>
    )
}

export default TopWriters