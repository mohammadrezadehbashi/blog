import React, { useEffect, useState } from "react";
import { useParams } from "react-router";
import parse from 'html-react-parser';
import Cookies from "universal-cookie";
import { ToastContainer, toast } from "react-toastify";
import RatingStar from "./RatingStar";


const cookies = new Cookies();

const SingleBlog = (props) => {

    
    const parse = require('html-react-parser');
    const { _id } = useParams();

    const [blog, setBlog] = useState(null);


    const [text, setText] = useState("");
    const [listComment, setListComment] = useState([]);

    const token = cookies.get("token")

    useEffect(() => {
        if (token === "") {
            toast.warning("با ورود  به سامانه امکان نظر دادن برای مقالات رو دارید")
        }
        getSingleBlog();
        getListComment()
    }, []);

    const getSingleBlog = async () => {
        try {
            fetch(`http://localhost:4000/blog/single-blog/${_id}`)
                .then(response => response.json())
                .then(data => {
                    console.log("datablogiss", data);
                    if (data._id) {
                        setBlog(data);
                    }
                })
        } catch (error) {
            console.error(error);
        }

    };

    /////////////////////////////////////get list comment -part two-

    const getListComment = async () => {
        try {
            const response = await fetch(`http://localhost:4000/comment/by-blog/${_id}`)

            const data = await response.json()

            console.log(data)
            setListComment(data)

        } catch (error) {
            console.error(error);
        }
    }


    ////////////////////////////////submit comment

    const handleComment = (e) => {
        setText(e.target.value);
    }

    const postComment = () => {
        const token = cookies.get("token")
        console.log("prepostComment", text);
        fetch("http://localhost:4000/comment/submit", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "auth": token ? `ut ${token}` : null
            },
            body: JSON.stringify({ text, blogId: _id })
        })
            .then(response => response.json())
            .then((data) => {
                setText(data)
                getListComment()
                setText("")
            })
    }

    if (!blog || !blog._id) return <h1>loading </h1>

    return (
        <>
            <div class="mb-20">

                {/* singleblog */}
                <p class="text-center text-3xl font-bold my-10">{blog.title}</p>
                <div class=" flex mx-10">
                    <div class="w-2/6 h-fit bg-cyan-200  flex justify-center " style={{height:"100px"}}>
                        <img src={blog.imgurl.substr(0, 4) === "http"||blog.imgurl.substr(0,4)==="data"  ? blog.imgurl : '/assets/images/icons8-image-100.png'} class="w-full h-full" />
                    </div>
                    <div class=" w-full bg-orange-100 px-10 py-5 ml-5 text-right">
                        <p class="text-right">
                            <span class="font-lg text-center text-xl">{parse(blog.content)}</span>
                        </p>
                    </div>
                </div>
            </div>
            {token &&
                        <>

            {/* score */}
            <div class="grid justify-center">
                <RatingStar _id={_id}/>
            </div>

            {/* comment */}
           
            <div class="ml-20 my-20 text-right">
                <p class="font-bold text-2xl">دیدگاه ها</p>
            </div>
            <div class="">
                <div class="grid grid-cols-6 ">
                    <div class="bg-sky-200 col-start-2 col-end-6 h-48 p-4 rounded-lg">
                        <textarea onChange={handleComment} value={text} placeholder="دیدگاه خود را وارد کنید"
                            class="p-5 w-full h-full text-right"></textarea>
                    </div>
                </div>
                <div class="flex justify-center mt-2">
                    <button class=" bg-green-400 h-10 rounded-md hover:bg-white border-2 border-green-400" onClick={postComment}>
                        <p class="px-6 py-2 text-center font-semibold">ارسال</p>
                    </button>
                </div>
            </div>
            </>
}
            {/* commentList*/}
            {listComment.map((item) => {
                return (
                    <div class="flex justify-between m-4 px-16 py-2 rounded-md bg-sky-300 border-2 border-red-900
                        sm:px-6 semi-sm:px-1 xs:m-1">
                        <div class="grid bg-rose-300 w-24 h-24 rounded-full overflow-hidden
                         semi-sm:w-12 semi-sm:h-12 semi-sm:my-2">
                            <img src={'./assets/images/The Mask wallpaperz.blog.ir .jpeg'} class="self-center h-full w-full" />
                        </div>

                        <div class=" bg-white w-full mr-4 pr-2 pt-2 sm:w-56 semi-sm:w-44">
                            <div class="" key={item._id}>
                                <p class="text-right text-md  font-semibold text-sky-900 self-center semi-sm:text-base">{item.text}</p>
                            </div>
                        </div>
                    </div>
                )
            })}

            <ToastContainer
                position="top-right"
                autoClose={2000}
                hideProgressBar={false}
                newestOnTop
                closeOnClick
                rtl={true}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="dark" />
        </>
    )
}

export default SingleBlog