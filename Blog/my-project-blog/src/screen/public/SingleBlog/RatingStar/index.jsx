import { useState } from "react";
import { FaStar } from "react-icons/fa";
import "./App.css";
import Cookies from "universal-cookie";
import { toast } from "react-toastify";

const cookies = new Cookies();

function RatingStar({_id}) {
    console.log("_iddd",_id);
    const [rating, setRating] = useState(null)
    const [hover, setHover] = useState(null)


    const rateSubmit = (currentRating) => {
        const token = cookies.get("token")
        console.log("ididid", _id)

        fetch("http://localhost:4000/blog/submit-rate", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'auth': token ? `ut ${token}` : null
            },
            body: JSON.stringify({ score: currentRating, blogId: _id })
        })

            .then((response => response.json()))
            .then(data => {
                toast(` امتیاز${currentRating} از طرف شما ثبت شد`)
                console.log("dataRate", data)

            })
    }

    return (
        <div>
            {[...Array(5)].map((star, index) => {
                const currentRating = index + 1;
                return (
                    <section className="app">
                        {/* <lable className="lable"> */}
                            <input type="radio"
                                name="rating"
                                value={currentRating}
                                // onClick={() => setRating(currentRating)}
                                onClick={() => rateSubmit(currentRating)}

                            />
                            <FaStar className="star" size={50}
                                color={currentRating <= (hover || rating) ? "#ffe100" : "#080807"}
                                onMouseEnter={() => setHover(currentRating)}
                                onMouseLeave={() => setHover(null)} />
                        {/* </lable> */}
                    </section>
                )
            })}

        </div>
    );
}

export default RatingStar;