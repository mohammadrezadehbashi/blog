import {configureStore} from '@reduxjs/toolkit'
import counterReducer from '../features/counter/counterSlice'
import topBlogReducer from '../features/topblog/topBlogSlice'

export const store =configureStore({
    reducer :{
        counter:counterReducer,
        posts:topBlogReducer
    },
})