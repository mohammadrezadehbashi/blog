import { createSlice } from "@reduxjs/toolkit"

initialState = {
    education: "",
    age: ""
}

function handleEducation(state) {
    state.education=education;
}

function handleAge(state) {
    state.age=age;
}

const informPerson = createSlice({
    name: "po",
    initialState,
    reducers: {
        handleEducation,
        handleAge
    }
})


export default informPerson