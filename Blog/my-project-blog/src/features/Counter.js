import React from "react";
import {useSelector,useDispatch} from 'react-redux'
import {decrementAction,incrementAction} from './counter/counterSlice'

export function Counter() {
    const count = useSelector((state) => state.counter.value)
    const dispatch = useDispatch()


return (
    <div>
        <div>
            <button onClick={() => dispatch(incrementAction())}>
                incerement
            </button>
            <span>{count}</span>
            <button onClick={() => dispatch(decrementAction())}>
                decreement
            </button>
        </div>
    </div>
)
}