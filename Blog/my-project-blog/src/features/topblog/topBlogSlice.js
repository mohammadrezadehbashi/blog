import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

export const getTopBlogg = createAsyncThunk('posts/fetch',
    async () => {
        const response = await fetch('http://localhost:4000/blog/top-blogs')
        const data = response.json();

        return data;
    }
)

const initialState = {
    postLists: [],
    fetchingPost: false,
    errorMassage: null
}

const topBlogSlice = createSlice({
    name: 'posts',
    initialState,
    extraReducers: {
        [getTopBlogg.fulfilled]:(state,action)=>{
            state.postLists = action.payload
            state.fetchingPost=false
        },
        [getTopBlogg.pending]:(state)=>{
            state.fetchingPost=true
        },
        [getTopBlogg.rejected]:(state)=>{
            state.fetchingPost=false
            state.errorMassage='this is error'
        }
}
})

export default topBlogSlice.reducer;