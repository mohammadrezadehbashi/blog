import { createSlice } from "@reduxjs/toolkit"


const initialState = {
    value: 0,
}

function increment(state) {
    state.value += 1
}

function decrement(state) {
    state.value -= 1
}

function incrementByAmount(state, action) {
    state.value += action.payload
}

const counterSlice = createSlice({
    name: 'counter',
    initialState,
    reducers: {
        increment,
        decrement,
        incrementByAmount
        // increment:(state)=>{
        //     state.value+=1
        // },
        // decrement:(state)=>{
        //     state.value-=1
        // },
        // incrementByAmount:(state,action)=>{
        //     state.value+=action.payload
        // },
    },
})

export const { 
    increment: incrementAction,
    decrement: decrementAction,
    incrementByAmount: incrementByAmountAction
} = counterSlice.actions

export default counterSlice.reducer