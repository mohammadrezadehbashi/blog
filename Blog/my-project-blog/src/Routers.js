import WebLayout from "./layout/WebLayout";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Home from "./screen/Home";
import SubmitBlog from "./screen/private/SubmitBlog";
import DashboardLayout from "./screen/Dashboard/DashboardLayout.js";
import EditBlog from "./screen/private/EditBlog";
import EditProfile from "./screen/private/EditProfile";
import BlogsMe from "./screen/private/BlogsMe";
import ListBlogs from "./screen/public/ListBlogs";
import ListWriters from "./screen/public/ListWriters";
import SingleBlog from "./screen/public/SingleBlog";
import SingleProWriter from "./screen/public/SingleProWriter";
import SignUp from "./screen/public/SignUp";
import Login from "./screen/public/Login";
import { Provider } from "react-redux";
import { store } from "./app/store";
function router(props) {
    return (
        <Provider store={store}>
            <BrowserRouter>
                <Routes>
                    {/* first layout public */}
                    <Route path="/" element={<WebLayout />}>
                        <Route path="" element={<Home />} />
                        <Route path="blogs" element={<ListBlogs />} />
                        <Route path="users" element={<ListWriters />} />
                        <Route path="blog/:_id" element={<SingleBlog />} />
                    </Route>

                    <Route>
                        <Route path="SignUp" element={<SignUp />} />
                    </Route>

                    <Route>
                        <Route path="Login" element={<Login />} />
                    </Route>

                    <Route>
                        <Route path="user/:_id" element={<SingleProWriter />} />
                    </Route>

                    {/* نستت رووت و اوت لت  و لایوت رو بخون داکیومنتش رو   */}
                    {/* second layout private */}
                    <Route path="/Dashboard/" element={<DashboardLayout />}>
                        <Route path="blog/submit" element={<SubmitBlog />} />
                        <Route path="blog/edit/:_id" element={<EditBlog />} />
                        <Route path="profile/edit" element={<EditProfile />} />
                        <Route path="myblogs" element={<BlogsMe />} />
                    </Route>
                </Routes>
            </BrowserRouter>
            {/* <App /> */}
        </Provider>
    );
}

export default router;