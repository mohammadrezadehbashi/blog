import React, { useContext,createContext, useState } from "react";
import {PostByUser} from "./screen/public/SingleProWriter"

const MyContext = createContext();

function ContextProvider({ children }) {

    // const func=PostByUser();
    // const [type, setType] = useState();

    const[toggleCoockie,setToggleCoockie]=useState(false);
    // function handleToggleCoockie(){
    //     setToggleCoockie(true);
    // }

   const Value={
        // func,
        // type,
        // setType
        toggleCoockie,
        setToggleCoockie
        // handleToggleCoockie
    }
    return (
        <MyContext.Provider value={Value}>
            {children}
        </MyContext.Provider>
    )
}

const useAuth=()=>useContext(MyContext)

export {ContextProvider,useAuth}
