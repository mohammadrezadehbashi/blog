import { Outlet } from "react-router-dom"
import Header from '../screen/component/Header';
import Footer from '../screen/component/Footer';

function WebLayout() {
  return (
    <div class="bg-stone-200">
      <Header />
      <Outlet />
      <Footer />
    </div>
  );
}

export default WebLayout;
