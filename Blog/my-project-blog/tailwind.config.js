module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    screens: {
      'mobile': '436px',
      // => @media (min-width: 640px) { ... }

      'tablet': '640px',
      // => @media (min-width: 640px) { ... }
      'semi-semi-lg-mobile': '799.98px',
      'laptop': '1024px',
      // => @media (min-width: 1024px) { ... }

      'desktop': '1280px',
      // => @media (min-width: 1280px) { ... }

      '2xl': { 'max': '1535px' },
      // => @media (max-width: 1535px) { ... }

      'xl': { 'max': '1279px' },
      // => @media (max-width: 1279px) { ... }

      "semi-xl": { 'max': '1100px' },
      // => @media (max-width: 867px) { ... }

      'lg': { 'max': '1023px' },
      // => @media (max-width: 1023px) { ... }

      "semi-lg": { 'max': '867px' },
      // => @media (max-width: 867px) { ... }

      "semi-semi-lg": { 'max': '799px' },
      // => @media (max-width: 867px) { ... }

      'md': { 'max': '767px' },
      // => @media (max-width: 767px) { ... }

      "semi-md": { 'max': '700px' },
      // => @media (max-width: 867px) { ... }

      'sm': { 'max': '639px' },
      // => @media (max-width: 639px) { ... }

      'up-semi-sm': { 'max': '551px' },
      // => @media (max-width: 639px) { ... }

      "semi-sm": { 'max': '435px' },
      // => @media (max-width: 867px) { ... }

      "kalak-semi-sm": { 'max': '434px' },
      // => @media (max-width: 867px) { ... }

      "xs": { 'max': '349px' },
      // => @media (max-width: 867px) { ... }
    },

    extend: {
      // backgroundImage: {
      //   'hero-pattern': "url('/src/assets/images/6460950-wallpaper-full-hd-1024x640.jpg')",       
      // }
    },
  },
  plugins: [],
}
